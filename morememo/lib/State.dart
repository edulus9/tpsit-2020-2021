import 'package:firebase_auth/firebase_auth.dart';

class AppState {
  //AppState single=_AppState();
  bool isLoading, isLogged, isInit;
  User user;


  AppState({
    this.isLoading = false,
    this.isLogged = false,
    this.isInit = false,
    this.user,
  });
}
