import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SharedWidget extends StatefulWidget {
  @override
  _SharedWidgetState createState() => _SharedWidgetState();
}

class _SharedWidgetState extends State<SharedWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.red, child: Center(child: Text(
      "ERROR! THIS FUNCTION IS UNIMPLEMENTED! \nPLEASE COME BACK LATER, OK?",
      style: TextStyle(fontSize: 15, color: Colors.yellow, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
    ),),);
  }
}
