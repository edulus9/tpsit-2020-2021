import 'dart:convert';

import 'package:flutter/foundation.dart';

class Food{
  String _name;
  Category _category;

  Food(String name, Category category){
    _name=name;
    _category=category;
  }

  Category get category => _category;

  set category(Category value) {
    _category = value;
  }

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  static Category _parseCategory(String s){
    switch(s){
      case "Primo":
        return Category.FirstCourse;
        break;
      case "Secondo":
        return Category.SecondCourse;
        break;
      case "Verdura":
        return Category.Vegetables;
        break;
      case "Dolce":
        return Category.Dessert;
        break;

    }
  }

  factory Food.fromJson(Map<String, dynamic> json){
    return Food(json['name'], parseCategory(json['category'].toString()));
  }

  static Category parseCategory(String c){
    c=c.toUpperCase();
    switch(c){
      case 'FIRSTCOURSE':
        return Category.FirstCourse;
        break;
      case 'SECONDCOURSE':
        return Category.SecondCourse;
        break;
      case 'VEGETABLES':
        return Category.Vegetables;
        break;
      case 'DESSERT':
        return Category.Dessert;
        break;

    }
  }



  @override
  String toString(){
    return _name + ' - ' + _category.toString();
  }
}

enum Category{
  FirstCourse, SecondCourse, Vegetables, Dessert
}