import 'package:flutter/material.dart';

import 'State.dart';

class DataWidget extends StatelessWidget {
  const DataWidget({Key key, this.appState, this.callback}) : super(key: key);

  final AppState appState;
  final Function callback;

  @override
  Widget build(BuildContext context){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
            padding: EdgeInsets.all(20.0),
            height: 150.0,
            width: 150.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: NetworkImage(appState.user.photoURL.toString()),
              ),
            )
        ),
        Text (
          'Hello, ' '${appState.user.displayName}' ' !',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.black, fontSize: 25),
        ),
        RaisedButton(
          child: Text("Logout"),
          onPressed: callback,
          color: Colors.red,
          textColor: Colors.yellow,
          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          splashColor: Colors.grey,
        )
      ],
    );
  }
}
