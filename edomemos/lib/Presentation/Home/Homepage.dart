import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

class HomePageWidget extends StatefulWidget {
  @override
  _HomePageWidgetLoaded createState() => _HomePageWidgetLoaded();
}

class _HomePageWidgetLoaded extends State<HomePageWidget> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).primaryColor,
            ),
          ],
        ),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15.0,
              vertical: 10,
            ),
            child: GNav(
              gap: 4,
              activeColor: Theme.of(context).cardColor,
              iconSize: 24,
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 5,
              ),
              duration: Duration(
                milliseconds: 800,
              ),
              tabBackgroundColor: Theme.of(context).primaryColor,
              tabs: [
                GButton(
                  icon: LineAwesomeIcons.home,
                  text: 'Home',
                ),
                GButton(
                  icon: LineAwesomeIcons.sticky_note_o,
                  text: 'All notes',
                ),
                GButton(
                  icon: LineAwesomeIcons.user,
                  text: 'Users',
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButtonAnimator: ,
    );
  }
}

class _HomePageWidgetLoading extends State<HomePageWidget> {


  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
