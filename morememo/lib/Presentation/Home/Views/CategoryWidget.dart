import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:morememo/Data/Models.dart';
import 'package:morememo/Domain/Constants.dart';
import 'package:morememo/Domain/Firestore.dart';

class CategoryWidget extends StatefulWidget {
  List<Category> catArr;
  CategoryWidget(this.catArr);
  @override
  _CategoryWidgetState createState() => _CategoryWidgetState(catArr);
}

class _CategoryWidgetState extends State<CategoryWidget> {
  List<Category> catArr;
  FirestoreHandler handler = FirestoreHandler.instance;

  _CategoryWidgetState(this.catArr);

  onPop() {
    Navigator.pop(context);
  }

  Widget builder(BuildContext context, int index) {
    return ListTile(
        leading: Text(catArr[index].name),
        title: ButtonBar(
          children: [
            IconButton(
                onPressed: (){onModify(catArr[index]);},
                icon: Icon(LineAwesomeIcons.edit)),
            IconButton(
                onPressed: (){onDelete(catArr[index]);},
                icon: Icon(LineAwesomeIcons.remove)),
          ],
        ));
  }

  onAdd() async{
    print("onAdd pressed.");
    TextEditingController nameController=TextEditingController();
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0),
              side: BorderSide(
                  color: Theme.of(context).primaryColor,
                  width: 1
              ),),
            title: Text("New category"),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        controller: nameController,
                        autocorrect: false,
                        cursorColor: Theme.of(context).primaryColor,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(15))),

                            contentPadding: EdgeInsets.all(10)
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child:
                    FloatingActionButton(
                    child: Icon(LineAwesomeIcons.check),
                    onPressed: () async {
                      Navigator.pop(context);
                      await handler.addCategory(nameController.text).then((value) {
                        print(value);
                        setState(() {
                          catArr.add(value);
                        });
                      });

                    },
                    //color: Theme.of(context).accentColor,
                  )),
                ],
              ),
              Text(""),
            ],
          );
        });
  }
  onDelete(Category c) {
    print("Calling deleteCategory");
    handler.deleteCategory(c);
    handler.getCategories().then((value){
      setState(() {
        catArr=value;
      });
    });
  }

  onModify(Category c) async {
    print("Calling modifyCategory");
    TextEditingController nameController=TextEditingController();
    nameController.text=c.name;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
      return SimpleDialog(
        title: Text("Modify category"),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                    controller: nameController,
                    autocorrect: false,
                    cursorColor: Theme.of(context).primaryColor,
                    decoration: InputDecoration(
                        fillColor: Theme.of(context).primaryColor,
                        hoverColor: Theme.of(context).primaryColor,
                        focusColor: Theme.of(context).primaryColor,
                        hintText: 'Name...'),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                  child: FloatingActionButton(
                child: Icon(LineAwesomeIcons.pencil),
                onPressed: () async {
                  Navigator.pop(context);
                    setState(() {
                      c.name=nameController.text;
                    });
                    handler.setCategory(c);
                },
                //color: Theme.of(context).accentColor,
              )),
            ],
          ),
          Text(""),
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.arrow_left),
          onPressed: onPop,
        ),
        centerTitle: true,
        title: Text("Edit Categories"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(borderRadius),
          bottomRight: Radius.circular(borderRadius),
        )),
      ),
      body: ListView.builder(
        itemBuilder: builder,
        itemCount: catArr.length,
      ),
      floatingActionButton: ButtonBar(
        children: [
          FloatingActionButton(heroTag: null,onPressed: onAdd, child: Icon(LineAwesomeIcons.plus),),
        ],
      ),
    );
  }
}
