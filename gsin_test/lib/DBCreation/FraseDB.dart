import 'dart:async';
import 'package:floor/floor.dart';
import 'package:gsin_test/DAOs/FraseCelebreDAO.dart';
import 'package:gsin_test/DAOs/UserDAO.dart';
import 'package:gsin_test/entities/FraseCelebreEntity.dart';
import 'package:gsin_test/entities/UserEntity.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
part 'FraseDB.g.dart'; // the generated code will be there


@Database(version: 1,entities: [FrasiCelebri, User])
abstract class FraseDB extends FloorDatabase
{
  FraseCelebreDAO get frasecelebreDao;

  UserDAO get userDao;
}