import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:morememo/Domain/Firestore.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

Future<User> loginWithGoogle() async{
  User user;
  if(_googleSignIn.currentUser != null) {
    FirestoreHandler.onAuthChange(_auth.currentUser);
    return _auth.currentUser;
  }
  GoogleSignInAccount googleUser;
  googleUser=await _googleSignIn.signIn();
  //while(googleUser==null) print("Ciao");
   //= _googleSignIn.signIn();
  GoogleSignInAuthentication googleAuth; //await googleUser.authentication;
  googleAuth=await googleUser.authentication;
  AuthCredential credential = GoogleAuthProvider.credential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );
  await _auth.signInWithCredential(credential);
  user = _auth.currentUser;
  //print("signed in " + user.displayName);
  FirestoreHandler.onAuthChange(user);
  return user;
}

Future<Null> logoutWithGoogle() async {
  await _auth.signOut();
  await _googleSignIn.signOut();
  FirestoreHandler.onAuthChange(null);
}

Future<User> loginWithCredentials(){

  FirestoreHandler.onAuthChange(null);
}

Future<User> signupWithCredentials(){

  FirestoreHandler.onAuthChange(null);
}



Future<Null> logoutWithCredentials() async {

  FirestoreHandler.onAuthChange(null);
}



/*
Future<Null> deleteUserWithGoogle(FirebaseUser user) async {
  await user.delete();
  await logoutWithGoogle();
}
*/
