import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import 'Models.dart';
import 'dart:io';

class Client {
  Conversation openConvo=null;
  File archiveFile;
  User u;
  bool remeberUser=false;
  static Client client=Client();
  List<Conversation> convos=[];
  Socket socket;
  Function() messageNotifier;

  Client(){
    Socket.connect("192.168.43.2", 3000).then((Socket sock) {
      socket = sock;
      socket.listen(messageHandler,
          onError: errorHandler, onDone: doneHandler, cancelOnError: false);
    })/*.catchError((Error e) {
      print("Unable to connect: $e");
    })*/;

    loadChats();
  }

  connectServer() async{
    if(socket==null){
      await Socket.connect("10.0.2.2", 3000).then((Socket sock) {
        socket = sock;
        socket.listen(messageHandler,
            onError: errorHandler, onDone: doneHandler, cancelOnError: false);
      })/*.catchError((Error e) {
      print("Unable to connect: $e");
    })*/;
    }

  }

  void messageHandler(data) {
    String message = utf8.decode(data).trim(), code;

    if (message.length >= 5) {
      code = message.substring(0, 5);
      message = message.substring(5);
    } else code = message.substring(0, message.length);
    print("MESSAGEEEEEEEEE $message");
    switch (code) {
      case "code0": //message
        try {
          handleMessageRecieved(Message.fromJSONString(message));
        } catch (FormatException) {
          print("Recieved Malformed json message. Discarding!");
          sendStatusMessage("Malformed message.", 4, null);
        }

        break;

      case "code3": //StatusMessage
        /*try*/ {
          handleStatusMessage(StatusMessage.fromJSONString(message));
        } /*catch (FormatException) {
          print("Recieved Malformed message. Discarding! $FormatException");
          sendStatusMessage("Malformed message.", 4, null);
        }*/
        break;

      default:
        print("Recieved Malformed message. Discarding!");
        sendStatusMessage("Malformed message.", 4, null);
    }

    //server.distributeMessage(this, '${_address}:${_port} Message: $message');
  }

  void errorHandler(Object error, StackTrace trace) {
    print(error);
    socket=null;
  }

  void doneHandler() {
    socket.destroy();
    socket=null;
    //exit(0);
  }

  void handleStatusMessage(StatusMessage status){
    StatusNotifier.singletonNotifier.notify(status);
    switch(status.statusCode){
      case 0: //
        break;
      case 1: //
        break;
      case 2: //authError
      u=null;
        break;
      case 3: //regError
        break;
      case 4: //
        break;
      case 5: //
        break;
    }
  }
  
  void handleMessageRecieved(Message m){
    if(!m.recipient.equals(this.u.toUserID())){
      print("Message recieved doesn't match user, please report to administrator!");
      return;
    }
    Conversation c = hasChat(m.sender);
    if(c==null){
      c=Conversation(m.sender);
      convos.add(c);
      c.add(m);
    }else{
      c.add(m);
    }
    if(openConvo==c){

    }else{
      c.read=false;
    }
    updateUsers();
    messageNotifier.call();
  }

  deleteConversation(Conversation c){
    convos.remove(c);
  }

  bool sendMessage(Message m, Conversation c){
    m.date=DateTime.now().toString();
    write("code0" + m.toJSONString());
    c.add(m);
    if(!convos.contains(c)){
      convos.add(c);
    }
    messageNotifier.call();
    updateUsers();
  }

  Conversation hasChat(UserID u){
    for(Conversation c in convos){
      if(c.userId.equals(u)) return c;
    }
    return null;
  }

  Future<bool> authenticate(String email, String password) async{
    u=User(email, password.hashCode);
    this.write('code1' + json.encode(User(email, password.hashCode)));
  }

  bool signUp(String email, String password){
    this.write('code2' + json.encode(User(email, password.hashCode)));
  }

  bool sendStatusMessage(String message, int statusCode, dynamic attachment){
    if(statusCode==5 && attachment==null) return false;
    this.write("code3"+StatusMessage(message, 0, attachment).toJson().toString());
    return true;
  }

  void write(String message){
    socket.write(message);
  }

  void loadChats() async {
    String response;
    Directory intStorage;

    try{
      await getExternalStorageDirectory().then((value) => intStorage=value);
      print("1");
      Directory('${intStorage.path}/Pistachio/').createSync();
      print("2");
      Directory('${intStorage.path}/Pistachio/ChatData/').createSync();
      print("3");
      archiveFile= File('${intStorage.path}/Pistachio/ChatData/Chats.json');
      await archiveFile.readAsString().then((value) => response=value);
    }catch(Exception){
      print("check failed");
      archiveFile= File('${intStorage.path}/Pistachio/ChatData/Chats.json');
      archiveFile.create();
      response=null;
    }
    print("4");
    if(response==null || response=="") return;
    final parsed = json.decode(response);

    List l=(json.decode(response) as List);
    for(var s in l){
      convos.add(Conversation.fromJson(s));
    }

    print(convos);
  }

  void updateUsers() async{
    archiveFile.deleteSync();
    archiveFile.createSync();
    await archiveFile.writeAsString(json.encode(convos));
  }


  debugChats(){
    print("PREVIOUS MESSAGES");
    for(int i=0; i<convos.length; i++){
      print(convos[i].toJson().toString());
    }

    convos.add(Conversation(UserID("e@e.e")));
    convos[0].add(Message("msg1-1", UserID("e@e.e"), UserID("u@u.u"), DateTime.now().toString()));
    convos[0].add(Message("msg2-1", UserID("e@e.e"),UserID("u@u.u"), DateTime.now().toString()));
    convos.add(Conversation(UserID("u@u.u")));
    convos[1].add(Message("msg1-2", UserID("u@u.u"),UserID("e@e.e"), DateTime.now().toString()));
    convos[1].add(Message("msg2-2", UserID("u@u.u"),UserID("e@e.e"), DateTime.now().toString()));

    print("SUCCEEDING MESSAGES");
    for(int i=0; i<convos.length; i++){
      print(convos[i].toJson().toString());
    }

    updateUsers();
  }
}

class StatusNotifier{
  static final singletonNotifier=new StatusNotifier();
  List<bool Function(StatusMessage)> observers=[];

  StatusNotifier(){}

  static StatusNotifier getNotifier(){
    return singletonNotifier;
  }

  attach(Function f){
    observers.add(f);
  }

  detach(Function f){
    observers.remove(f);
  }

  notify(StatusMessage m){
    print("Notifying of changes...");
    for(bool Function(StatusMessage) fun in observers) {
      if(fun(m)) fun=null;
    }
    observers.remove(null);
  }
}