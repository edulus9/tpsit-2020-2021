import 'package:flutter/material.dart';
import 'package:my_menu/features/myMenu/Data/Food.dart';
import 'package:my_menu/features/myMenu/Data/ServerHandler.dart';

class SettingsWidget extends StatefulWidget {
  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget>{

  void meth() async{
    ServerHandler s = ServerHandler();
    List<Food> l;

    await s.fetchMenu('http://10.0.2.2:3000/dailymenu.json').then((value) => l=value);
    for(int i=0; i<l.length; i++){
      print('\n ' + l[i].toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
              padding: const EdgeInsets.all(8),
              children: [
                Card(
                elevation: 8.0,
                margin: new EdgeInsets.symmetric(
                    horizontal: 10.0, vertical: 6.0),
                child: Container(
                  decoration: BoxDecoration(),
                  child: ListTile(
                    contentPadding: EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10.0),
                    leading: Container(
                      padding: EdgeInsets.only(right: 12.0),
                      decoration: new BoxDecoration(
                          border: new Border(
                              right: new BorderSide(
                                  width: 1.0))),
                      child: Icon(Icons.search_rounded),
                    ),
                    title: Text(
                      'henlo',
                      style: Theme.of(context)
                          .textTheme
                          .headline5,
                    ),
                  ),
                ))

            ],).build(context),

    );
  }
}