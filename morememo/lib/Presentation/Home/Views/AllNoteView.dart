import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:morememo/Data/Models.dart';
import 'package:morememo/Data/Note.dart';
import 'package:morememo/Domain/Constants.dart';
import 'package:morememo/Domain/Firestore.dart';
import 'package:morememo/Presentation/Home/Views/SingleNoteView.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'EditNote.dart';

class AllNoteWidget extends StatefulWidget {
  @override
  _AllNoteWidgetState createState() => _AllNoteWidgetState();
}

class _AllNoteWidgetState extends State<AllNoteWidget> {
  FirestoreHandler handler=FirestoreHandler.instance;
  List<Note> notes;
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  _AllNoteWidgetState(){
    initState();
  }

  onAdd()async {
    Note n;
    await handler.addNote(
        Note("", "", "", null, [], DateTime.now(),DateTime.now(), null)
    ).then((value) => n=value);
    setState(() {
      Navigator.push(context, MaterialPageRoute(builder: (context) => EditNote(n))).then((value) {setState(() {

      });});
    });

  }

  initState(){
  }

  Widget builder(BuildContext context, int index){
    return SingleNoteWidget(notes[index]);
  }

  void _onRefresh() async{
    // monitor network fetch
    //await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
    });
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  /*void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    //items.add((items.length+1).toString());
    if(mounted)
      /*he did a setstate*/
    _refreshController.loadComplete();
  }*/

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: handler.getNotes(),
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          print(snapshot.stackTrace.toString() + "\nERROR: "+ snapshot.error.toString());
          return Center(child: Icon(Icons.error),);
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          notes=snapshot.data;
          return Scaffold(
            appBar: (notes.isEmpty) ? AppBar(
              backgroundColor: Colors.transparent,
              shadowColor: Colors.transparent,
              leading: Icon(null),
              centerTitle: true,
              title: Text("😿 Whoa, such empty!"),

            ) : null,
            floatingActionButton: FloatingActionButton(heroTag: null, onPressed: onAdd, child: Icon(LineAwesomeIcons.plus),),
              body: SmartRefresher(
                  enablePullDown: true,
                  //enablePullUp: true,
                  header: WaterDropHeader(),
                  /*footer: CustomFooter(
                    builder: (BuildContext context,LoadStatus mode){
                      Widget body ;
                      if(mode==LoadStatus.idle){
                        body =  Text("pull up load");
                      }
                      else if(mode==LoadStatus.loading){
                        body =  SpinKitCircle(color: Theme.of(context).accentColor,);
                      }
                      else if(mode == LoadStatus.failed){
                        body = Text("Load Failed! Click retry!");
                      }
                      else if(mode == LoadStatus.canLoading){
                        body = Text("release to load more");
                      }
                      else{
                        body = Text("No more Data");
                      }
                      return Container(
                        height: 55.0,
                        child: Center(child:body),
                      );
                    },
                  ),*/
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  //onLoading: _onLoading,
                  child: ListView.builder(itemCount: notes.length, itemBuilder: builder))
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return Center(child: Container(child: SpinKitChasingDots(color: Theme.of(context).accentColor,)));
      },
    );
    /**/
  }
}
