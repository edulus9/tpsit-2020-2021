import 'package:flutter/material.dart';

import 'features/myMenu/Presentation/Homepage.dart';

void main() {
  runApp(MenuApp());
}

class MenuApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      darkTheme: ThemeData.dark(),
      theme: ThemeData(
        // is not restarted.
        primarySwatch: Colors.teal,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomepageWidget(title: 'Menù Application'),
      debugShowCheckedModeBanner: false,
    );
  }
}


