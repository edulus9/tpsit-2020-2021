import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:morememo/Data/Models.dart';
import 'package:morememo/Data/Note.dart';

class FirestoreHandler{
  static FirestoreHandler instance=FirestoreHandler._();
  FirebaseFirestore firestore=FirebaseFirestore.instance;
  FirebaseAuth auth=FirebaseAuth.instance;
  String root;
  //static StreamSubscription authStreamController=FirebaseAuth.instance.authStateChanges().listen((event){onAuthChange(event);});
  //static StreamSubscription userStreamController=FirebaseAuth.instance.userChanges().listen((event){onAuthChange(event);});


  FirestoreHandler._(){
    if(auth.currentUser!=null){
      root = "UserData/" + auth.currentUser.uid;
      //print("BUILDING NEW DBBBBBBBB");
      initUser();
    }
  }

  initUser()async{
    print("executing inituser...");
    bool accExists=null;
    await firestore.doc(root).get().then((value)=>
    accExists=value.exists
    );
    if(accExists) {
      print("Account exists");
      return;
    }
    print("Account DOES NOT exist");
    accExists=null;
    firestore.collection("UserData/").doc(auth.currentUser.uid).set({}).then((value) async {
      DocumentReference defCategory;
      //firestore.doc(root).collection("Tags").add({"Name": "None"});
      //await firestore.doc(root).collection("Categories").add({"Name": "None"}).then((value) => defCategory=value);

      /*firestore.doc(root).collection("Notes").add({
        "Title": "Sample note",
        "Content": "",
        "DateCreated": Timestamp.fromDate(DateTime.now()),
        "DateLastModified": Timestamp.fromDate(DateTime.now()),
        "Category": defCategory,
        "Tags": [],
        "Shared": [],
      });*/
    });
  }

  Future<List<Category>> getCategories() async{
    List<QueryDocumentSnapshot> docList;
    List<Category> tempCategories=[];
    await firestore.collection(root + "/Categories").get().then((value) => docList=value.docs);
    for(QueryDocumentSnapshot d in docList){
      tempCategories.add(Category(d.get("Name"), d.id, d.reference));
    }
    return tempCategories;
  }
  Future<List<Tag>> getTags() async{
    List<QueryDocumentSnapshot> docList;
    List<Tag> tempCategories=[];
    await firestore.collection(root + "/Tags").get().then((value) => docList=value.docs);
    for(QueryDocumentSnapshot d in docList){
      tempCategories.add(Tag(d.get("Name"), d.id, d.reference));
    }
    return tempCategories;
  }

  static onAuthChange(User u){
    print("****onUserChanged: "+FirebaseAuth.instance.currentUser.toString());
    if(u!=null) instance=FirestoreHandler._();
  }



  Future<Note> addNote(Note n) async {
    await firestore.doc(root).collection("Notes").add({
      "Title": n.title,
      "Content": n.content,
      "DateCreated": Timestamp.fromDate(DateTime.now()),
      "DateLastModified": Timestamp.fromDate(DateTime.now()),
      "Category": (n.category!=null)?n.category.reference: null,
      "Tags": [],
      "Shared": [],
    }).then((value) => n.reference=value);
    return n;
  }

  Future<Tag> addTag(String name)async{
    Tag c;
    await firestore.doc(root).collection("Tags").add({"Name": name}).then((value) async=> await value.get().then((v) => c=Tag(name, value.id, v.reference)));
    return c;
  }

  Future<Category> addCategory(String name) async {
    Category c;
    await firestore.doc(root).collection("Categories").add({"Name": name}).then((value) async=> await value.get().then((v) => c=Category(name, value.id, v.reference)));
    return c;
  }

  Future setCategory(Category c) async {
    await c.reference.set({
      "Name": c.name,
    });
  }
  Future setTag(Tag c) async {
    await c.reference.set({
      "Name": c.name,
    });
  }

  deleteTag(Tag t) async {
    DocumentSnapshot d;
    await t.reference.delete();
    List<QueryDocumentSnapshot> l;
    List tagRefs;


    try{
      await firestore.collection("$root/Notes").where("Tags", arrayContains: t.reference).get().then((value) => l=value.docs);

      for(QueryDocumentSnapshot q in l){
        tagRefs=q.get("Tags");
        tagRefs.remove(t.reference);

        q.reference.set({
          "Title": q.get("Title"),
          "Content": q.get("Content"),
          "Category": q.get("Category"),
          "DateLastModified": q.get("DateLastModified"),
          "DateCreated": q.get("DateCreated"),
          "Shared": q.get("Shared"),
          "Tags": tagRefs,
        });
      }

    }catch(Exception){
      print(Exception);
    }

    return;
  }
  deleteCategory(Category t) async {
    List<QueryDocumentSnapshot> l;
    await t.reference.delete();

    try{
      await firestore.collection("$root/Notes").where("Category", isEqualTo: t.reference).get().then((value) => l=value.docs);

      for(QueryDocumentSnapshot q in l){q.reference.set({
          "Title": q.get("Title"),
          "Content": q.get("Content"),
          "Category": null,
          "DateLastModified": q.get("DateLastModified"),
          "DateCreated": q.get("DateCreated"),
          "Shared": q.get("Shared"),
          "Tags": q.get("Tags"),
        });
      }

    }catch(Exception){
      print(Exception);
    }

    return;
  }

  editCategory(Category c) async {
    await c.reference.set({
      "Name": c.name
    });
    return;
  }
  editTag(Tag c) async {
    await c.reference.set({
      "Name": c.name
    });
    return;
  }

  Future<Category> getCategory(String uid) async{
    DocumentSnapshot d;
    await firestore.doc("$root/Categories/$uid").get().then((value) => d=value);
    return Category(d.get("Name"), d.id, d.reference);
  }
  Future<DocumentReference> getCategoryReference(String uid) async{
    DocumentSnapshot d;
    await firestore.doc("$root/Categories/$uid").get().then((value) => d=value);
    return d.reference;
  }
  Future<DocumentReference> getCategorybyName(String name) async{
    QuerySnapshot d;
    await firestore.collection("$root/Categories")
        .where('Name', isEqualTo: name)
        .get().then((value) => d=value);
    return d.docs[0].reference;
  }

  Future<Tag> getTag(String uid) async{
    DocumentSnapshot d;
    await firestore.doc("$root/Tags/$uid").get().then((value) => d=value);
    return Tag(d.get("Name"), d.id, d.reference);
  }

  Future<List<Note>> getNotes() async{
    List<QueryDocumentSnapshot> l;
    List<Note> result=[];
    print(root);
    try{
      await firestore.collection("$root/Notes").get().then((value) => l=value.docs);

    }catch(Exception){
      print(Exception);
    }

    for(QueryDocumentSnapshot q in l){
      await getNoteByReference(q.reference).then((value) => result.add(value));
    }

    result.sort((Note a, Note b){
      if(a.dateLastModified.isAfter(b.dateLastModified)) return -1;
      if(a.dateLastModified==b.dateLastModified) return 0;
      else return 1;
    });
    return result;
  }

  Future<Note> getNote(String uid)async{
    DocumentSnapshot d, category;
    await firestore.doc("$root/Notes/$uid").get().then((value) => d=value);
    await d.get("Category").get().then((value) => category=value);
    Note n = Note(
      d.id,
      d.get("Title"),
      d.get("Content"),
      Category(category.get("Name"), category.id, d.get("Category")),
      d.get("Tags"),
      d.get("DateLastModified").toDate(),
      d.get("DateCreated").toDate(),
      d.reference
    );
    return n;
  }
  Future<Note> getNoteByReference(DocumentReference ref)async{
    DocumentSnapshot d, category;
    Category c;
    await ref.get().then((value) => d=value);
    try{
      await d.get("Category").get().then((value) => category=value);
      c=Category(category.get("Name"), category.id, d.get("Category"));
  }catch(Error){}
    return Note(
      d.id,
      d.get("Title"),
      d.get("Content"),
      c,
      d.get("Tags"),
      d.get("DateLastModified").toDate(),
      d.get("DateCreated").toDate(),
      d.reference
    );
  }

  Future deleteNote(DocumentReference n) async{
    await n.delete();
    return;
  }

  Future updateNote(Note n) async {
    await n.reference.set({
      "Title": n.title,
      "Content": n.content,
      "Category": (n.category!=null) ? n.category.reference :null,
      "DateLastModified": Timestamp.now(),
      "DateCreated": Timestamp.fromDate(n.dateCreated),
      "Shared": n.shared,
      "Tags": n.tags,
    }
    );
  }

  Future<bool> isSharingEnabled() async {
    bool b;
    await firestore.doc("ShareUsers/"+auth.currentUser.email).get().then((value) => b=value.exists);
    return b;
  }

  Future disableSharing() async{
    firestore.collection("Users/").doc(auth.currentUser.email).delete();
  }

  Future enableSharing() async{
    await firestore.collection("Users/").doc(auth.currentUser.email).set({"UID": auth.currentUser.uid});
  }

  Future setSharing(bool b) async {
    bool state;
    await isSharingEnabled().then((value) => state=value);
    if(state!=b) {
      if (state) {
        await enableSharing();
      } else {
        disableSharing();
      }
    }
    return;
  }

}