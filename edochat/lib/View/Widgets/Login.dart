import 'package:edochat/Model/Client.dart';
import 'package:edochat/Model/Models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'AppbarWidget.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  Client client;
  TextEditingController userController=TextEditingController();
  TextEditingController passController=TextEditingController();
  String authErrorMessage = "Authentication Error. Wrong credentials?";
  bool errorVisibility = false, loadingVisibility = false;
  String username, password;

  _LoginState() {
    client=Client.client;
  }

  void error(String e){
    setState(() {
      authErrorMessage=e;
      loadingVisibility = false;
      errorVisibility = true;
    });
  }

  bool credentialsCheck(String u, String p){
    if(u==null) {
      error("Empty email");
      return false;
    }
    if(p==null) {
      error("Empty password");
      return false;
    }
    if(u=="") {
      error("Empty email");
      return false;
    }
    if(p=="") {
      error("Empty password");
      return false;
    }
    if(p.length<5) {
      error("Password too short");
      return false;
    }
    if(!(u.contains("@") && u.contains("."))){
      error("Not an e-mail address");
      return false;
    }
      return true;
    }

    printChats(){
      print("CONVOSSSSSS");
    for(Conversation c in client.convos){
      print(c.toString());
    }

      client.debugChats();
    Navigator.popAndPushNamed(context, "/convos");
    }


  bool loginConfirmationRecieved(StatusMessage m) {
    print("confirmation recieved!");
    if (m.statusCode == 3 || m.statusCode == 2) {
      //Something's wrong i can feel it...
      print("somethings wrong");
      error("Authentication error");
      return true;
    } else if (m.statusCode == 1) {
      //It's ok!
      setState(() {
        loadingVisibility = false;
      });
      print("All clear log in");
      Navigator.popAndPushNamed(context, "/convos");
      return true;
    }
    return false;
    StatusNotifier.singletonNotifier.detach(loginConfirmationRecieved);
  }

  void toConv(){
    Navigator.popAndPushNamed(context, "/convos");
  }

  bool signupConfirmationRecieved(StatusMessage m) {
    print("confirmation recieved!");
    if (m.statusCode == 3 || m.statusCode == 2) {
      //Something's wrong i can feel it...
      print("somethings wrong signup");
      error("Already signed up.");
      return true;
    } else if (m.statusCode == 1) {
      //It's ok!
      setState(() {
        loadingVisibility = false;
      });
      print("All clear signup");
      return true;
    }
    return false;
  }


  void onSignup() {
    if(!credentialsCheck(userController.text, passController.text)) return;
    setState(() {
      loadingVisibility = true;
    });
    Future.delayed(Duration(seconds: 2)).then((value) {
      StatusNotifier.singletonNotifier.attach(signupConfirmationRecieved);
      client.signUp(userController.text, passController.text);
    });
  }

  onLogin() async {
    if(!credentialsCheck(userController.text, passController.text)) return;
    setState(() {
      loadingVisibility = true;
    });
    Future.delayed(Duration(seconds: 2)).then((value) {
      StatusNotifier.singletonNotifier.attach(loginConfirmationRecieved);
      client.authenticate(userController.text, passController.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        backgroundColor: Theme.of(context).accentColor,
        /*appBar: appBarWidget(DropdownButton(
          icon: Icon(null),
        )),*/
        body: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 80),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 50,
                    height: 50,
                    child: Image.asset("assets/images/pista-512.png"),
                  ),
                    TextField(
                      controller: userController,
                      autocorrect: false,
                      decoration: InputDecoration(
                          icon: Icon(Icons.email_rounded),
                          hintText: 'Username'),
                    ),
                    TextField(
                      controller: passController,
                      autocorrect: false,
                      obscureText: true,
                      decoration: InputDecoration(
                          icon: Icon(Icons.vpn_key_rounded),
                          hintText: 'Password'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Visibility(
                          visible: errorVisibility,
                          child: Text(authErrorMessage,
                              style: TextStyle(color: Colors.red)),
                        ),
                        SizedBox(
                          height: 50,
                          width: 50,
                          child: Visibility(
                            visible: loadingVisibility,
                            child: SpinKitChasingDots(
                                color: Theme.of(context).primaryColor,
                                size: 50.0),
                          ),
                        )
                      ],
                    ),
                    ElevatedButton(
                      //LogIn
                      style: ElevatedButton.styleFrom(
                          primary: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0))),
                      child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              'Log In',
                              style: Theme.of(context).textTheme.headline4,
                            ),
                          )),
                      onPressed: onLogin,
                    ),
                    ElevatedButton(
                      //SignUp
                      style: ElevatedButton.styleFrom(
                          primary: Theme.of(context).accentColor,
                          onPrimary: Theme.of(context).accentColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0))),
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Sign Up',
                            style: Theme.of(context).textTheme.headline4,
                          ),
                        ),
                      ),
                      onPressed: onSignup,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
