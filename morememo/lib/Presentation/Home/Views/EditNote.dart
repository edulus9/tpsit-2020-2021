import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown_editor/flutter_markdown_editor.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:markdown_editable_textinput/markdown_text_input.dart';
import 'package:morememo/Data/Models.dart';
import 'package:morememo/Data/Note.dart';
import 'package:morememo/Domain/Constants.dart';
import 'package:morememo/Domain/Firestore.dart';

class EditNote extends StatefulWidget {
  Note note;
  @override
  EditNote(this.note) {}
  _EditNoteState createState() => _EditNoteState(note);
}

class _EditNoteState extends State<EditNote> with TickerProviderStateMixin {
  Note note;
  FirestoreHandler handler = FirestoreHandler.instance;
  TextEditingController titleController;
  Category catSelection;
  List<Tag> tags;
  List<Category> categories;
  Widget dropdown;
  TextEditingController contentController;

  _EditNoteState(this.note) {
    catSelection = note.category;
    contentController = TextEditingController(text: note.content);
    titleController = TextEditingController(text: note.title);
    dropdown = Container(
      width: 30,
    );
    handler.getCategories().then((value) {
      categories = value;
      setState(() {});
    });
  }

  onPop() {
    note.content = contentController.text;
    note.title = titleController.text;
    print(note);
    handler.updateNote(note).then((value) {
      Navigator.pop(context);
    });
  }

  chooseTags() async {
    print("chooseTags called.");
    await handler.getTags().then((value) => this.tags = value);
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
              side: BorderSide(color: Theme.of(context).primaryColor, width: 1),
            ),
            title: Text("Choose tags"),
            children: [
              Container(
                  height: 300,
                  width: 100,
                  child: ListView.builder(
                      itemBuilder: tagBuilder, itemCount: tags.length)),
              Align(
                alignment: Alignment.bottomLeft,
                child: FloatingActionButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Icon(LineAwesomeIcons.close),
                ),
              )
            ],
          );
        });
  }

  onCategoryChanged(Category c) {
    setState(() {
      catSelection = c;
      note.category = c;
    });
  }

  List<DropdownMenuItem<Category>> categoryBuilder() {
    List<DropdownMenuItem<Category>> l = [];
    l.add(DropdownMenuItem<Category>(
        onTap: () {
          setState(() {
            catSelection = null;
          });
        },
        value: null,
        child: Text(
          "None",
          style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color, fontStyle: FontStyle.italic),
        )));
    for (Category c in categories) {
      l.add(DropdownMenuItem<Category>(
        onTap: () {
          setState(() {
            catSelection = c;
          });
        },
        value: c,
        child: Text(
          c.name,
          style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color),
        ),
      ));
      //print(l.last.child);
    }
    return l;
  }

  Widget tagBuilder(BuildContext context, int index) {
    Tag t = tags[index];
    bool check = note.tags.contains(t.reference);

    return StatefulBuilder(
      builder: (context, _setState) => ListTile(
          leading: Checkbox(
            value: check,
            onChanged: (bool b) {
              if (b) {
                if (!note.tags.contains(t.reference))
                  note.tags.add(t.reference);
              } else {
                note.tags.remove(t.reference);
              }
              _setState(() {
                check = b;
              });
            },
          ),
          title: Text(t.name)),
    );

    return ListTile(
        leading: Checkbox(
          value: check,
          onChanged: (bool b) {
            setState(() {
              if (b) {
                if (!note.tags.contains(t.reference))
                  note.tags.add(t.reference);
              } else {
                note.tags.remove(t.reference);
              }
              check = b;
            });
          },
        ),
        title: Text(t.name));
  }

  getDropdown() {
    if (categories == null) return Container();
    return Container(
      child: DropdownButton<Category>(
        value: catSelection,
        //icon: const Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        style: TextStyle(),
        underline: Container(
          height: 0,
          //color: Colors.deepPurpleAccent,
        ),
        onChanged: (Category c) {
          onCategoryChanged(c);
        },
        items: categoryBuilder(),
      ),
    );
  }

  onDelete() {
    Navigator.pop(context);
    if(note.reference!=null){
      handler.deleteNote(note.reference).then((value) {
        Navigator.pop(context);
      });
    }

  }

  Widget mdTest() {
    MarkDownEditor markDownEditor =
        MarkDownEditor(controller: contentController);
    return Column(
      children: [
        markDownEditor.field,
        markDownEditor.preview,
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.arrow_left),
          onPressed: onPop,
        ),
        title: TextField(
          controller: titleController,
          showCursor: true,
          decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              contentPadding: EdgeInsets.all(10)),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(borderRadius),
          bottomRight: Radius.circular(borderRadius),
        )),
        actions: [
          getDropdown(),
          Padding(
            padding: const EdgeInsets.all(3.0),
            child: PopupMenuButton(
              icon: Icon(LineAwesomeIcons.ellipsis_v),
              itemBuilder: (context){
                return <PopupMenuEntry>[
                  PopupMenuItem(
                    child: TextButton(
                      child: Text("Delete"),
                      onPressed: onDelete,
                    ),
                  ),
              ];},
            ),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        //child: MarkdownTextInput(onNoteContentEdit, note.content)
        child: mdTest(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(LineAwesomeIcons.tag),
        onPressed: chooseTags,
      ),
    );
  }
}
