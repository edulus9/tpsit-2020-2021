import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'Food.dart';


class ServerHandler{
  static final ServerHandler _singleton=ServerHandler._internal();
  List<Food> _foodList;
  bool isDownloading;


  ServerHandler._internal(){
  }

  factory ServerHandler(){
    return _singleton;
  }

  Future<http.Response> _retrieveMenuJSON(String URL) async{
    return http.get(URL);
  }

  Future<List<Food>> fetchMenu(String URL) async {
    final response = await _retrieveMenuJSON(URL);

    if(response.statusCode==200){
      return _parseFood(response.body);
    }
    throw Exception('Failed to load body!!');
  }

  Future<List<Food>> _parseFood(String responseBody) async{
    List<dynamic> parsed = jsonDecode(responseBody);
    _foodList = List(parsed.length);

    for(int i=0; i<parsed.length; i++){
      _foodList[i]=Food.fromJson(parsed[i]);
    }

    return _foodList;
  }

  Future Download(String URL){
    isDownloading=true;
    fetchMenu(URL).then((value){_foodList=value; isDownloading=false;}).catchError(_onError);
    
  }

  bool isDownloaded()=> _foodList!=null;

  List<Food> getFoods(){
    if(_foodList==null) throw http.ClientException('Download not initiated');
    return _foodList;
  }

  void deleteCache(){
    _foodList=null;
  }

  _onError(){
    isDownloading=false;
  }

}
