import 'package:edochat/View/Widgets/ConversationListWidget.dart';
import 'package:flutter/material.dart';

import 'View/Widgets/Login.dart';

void main() {
  runApp(EdoChat());
}

class EdoChat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EdoChat',
      darkTheme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green,
        primaryColor: Colors.green[100],
        accentColor: Colors.green[50],

        primaryColorDark: Colors.green[50],

        visualDensity: VisualDensity.adaptivePlatformDensity,),
      initialRoute: "/login",
      routes: {
        "/convos": (BuildContext context) => ConversationListWidget(),
        "/login": (BuildContext context) => Login(),
      },
    );
  }
}


/*import 'package:flutter/material.dart';

void main() {
  runApp(Nav2App());
}

class Nav2App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: FlatButton(
          child: Text('View Details'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return DetailScreen();
              }),
            );
          },
        ),
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: FlatButton(
          child: Text('Pop!'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}*/