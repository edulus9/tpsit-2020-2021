import 'package:shared_preferences/shared_preferences.dart';

class SettingsManager{
  static final String URLPreference='url';


  static Future<SharedPreferences> getSharedPreferencesInstance() async {
    return await SharedPreferences.getInstance();
  }

  static void setKV(String key, dynamic value) async {
    SharedPreferences sharedPreferences = await getSharedPreferencesInstance();
    if(value is String){
      sharedPreferences.setString(key, value);
    }
  }

  static void resetSharedPreferences(List<String> list) async{
    SharedPreferences sharedPreferences = await getSharedPreferencesInstance();
    for(String key in list)
      sharedPreferences.remove(key);
  }
}