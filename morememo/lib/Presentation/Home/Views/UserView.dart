import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:morememo/GSIn.dart';

class UserWidget extends StatefulWidget {

  @override
  _UserWidgetState createState() => _UserWidgetState();
}

class _UserWidgetState extends State<UserWidget> {
  String email=FirebaseAuth.instance.currentUser.email;
  String name=FirebaseAuth.instance.currentUser.displayName.substring(0, FirebaseAuth.instance.currentUser.displayName.indexOf(" "));
  bool darkSwitch=false, shareSwitch=false;

  void onLogout(){
    logoutWithGoogle();
    Navigator.popAndPushNamed(context, "/login");
  }

  changeDark(bool b){

  }

@override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(onPressed: onLogout, child: Icon(Icons.logout),),
      body: ListView(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
            child: Card(
              shape: RoundedRectangleBorder(side: BorderSide(
                  color: Theme.of(context).primaryColor,
                  width: 1
              ),
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                child: Container(
                  width: double.infinity,
                    height: 70,
                    child: Stack(
                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      //height: double.infinity,
                      alignment: Alignment.bottomRight,
                      child: Text(email, style: Theme.of(context).textTheme.caption,),
                    ),Container(
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.topRight,
                      child: Text("Hey, $name!", style: Theme.of(context).textTheme.headline6,),
                    ),
                    Container(
                        padding: EdgeInsets.all(20.0),
                        height: 70.0,
                        width: 70.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(FirebaseAuth
                                .instance.currentUser.photoURL
                                .toString()),
                          ),
                        )),
                  ],
                )),
              ),
            ),
          ),
          ListTile(
            leading: Switch(
              value: darkSwitch,
              onChanged: changeDark,
            ),
            title: Text("Dark mode"),
          ),
          ListTile(
            leading: Switch(
              value: darkSwitch,
            ),
            title: Text("Note sharing enables"),
          ),
        ],
      ),
    );
  }
}
