import 'package:floor/floor.dart';
import 'package:gsin_test/entities/UserEntity.dart';

@dao
abstract class UserDAO {

  @Query("SELECT * FROM Users")
  Future<List<User>> getAllUsers();

  @insert
  Future<void> insertUser(User user);

}