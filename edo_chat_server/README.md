# edo_chat_server

Server for EdoChat application

## EdoRest protocol
I have invented an ad-hoc protocol to send simple messages. I've called it EdoRest protocol due to its similarity with the rest protocol.
#PacketCodes
Code*(JSONObjectType)

code0(message): Message
code1(User): Authentication request
code2(User): Registration Request
code3(StatusCode): StatusCode object
ls: debug command that lists all users registered and connected 

-------

#StatusCodes
0: Generic error
1: Operation successful
2: authentication Error (Will void authentication if sent to a client while authenticated, used in message and authentication form responses)
3: registration Error 
4: formatting Error (Sent due to malformed messages to clients and server)
5: message sending Error, in this case the attachment will be the message (Either the message is not )
6: 

