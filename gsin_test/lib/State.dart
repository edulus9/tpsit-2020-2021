import 'package:firebase_auth/firebase_auth.dart';

class AppState {
  bool isLoading, isLogged;
  FirebaseUser user;

  AppState({
    this.isLoading = false,
    this.isLogged = false,
    this.user,
  });
}
