//import 'dart:html';

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

class HomePageWidget extends StatefulWidget {
  @override
  _HomePageWidgetState createState() => _HomePageWidgetState();
}

class _HomePageWidgetState extends State<HomePageWidget> {
  int _selection = 1;

  Widget _getMainWidget() {
    switch(_selection){
      case 0:
        return getHomeWidget();
        break;
      case 1:
        break;
      case 2:
        break;
    }
  }

  Widget getHomeWidget() {
    return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          padding: EdgeInsets.all(24),
          child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(

                  padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset("assets/boots.png", width: 50, height: 50,),
                    Text("È meglio se ti metti \nXe mejo", textAlign: TextAlign.left,),

                  ],
                ),
                  )),
        )
      ],
    ));
  }

  Widget getStationWidget() {}

  _onItemTapped(int number) {
    setState(() {
      _selection=number;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: _getMainWidget(),
      bottomNavigationBar:
      Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(.1))
        ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
            child: GNav(
                gap: 8,
                activeColor: Colors.white,
                iconSize: 24,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 800),
                tabBackgroundColor: Colors.grey[800],
                tabs: [
                  GButton(
                    icon: Icons.home,
                    text: 'Home',
                  ),
                  GButton(
                    icon: Icons.shield,
                    text: 'Mose',
                  ),
                  GButton(
                    icon: Icons.waves,
                    text: 'Stazioni',
                  ),
                  GButton(
                    icon: Icons.timeline,
                    text: 'Previsioni',
                  ),
                ],
                selectedIndex: _selection,
                onTabChange: (index) {
                  setState(() {
                    _selection = index;
                  });
                }),
          ),
        ),
      ),
      /*BottomNavigationBar(
        currentIndex: _selection,
        onTap: _onItemTapped,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined), label: "Home"),
          BottomNavigationBarItem(icon: Icon(Icons.shield), label: "Mose"),

          BottomNavigationBarItem(
              icon: Icon(Icons.waves_rounded), label: "Stazioni"),
          BottomNavigationBarItem(
              icon: Icon(Icons.waves_rounded), label: "Previsioni"),
        ],
      ),*/
    );
  }
}
