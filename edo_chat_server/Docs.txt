code1{"email":"e@e.e","passwordHASH": 6232532}
code1{"email":"u@u.u","passwordHASH": 6232532}




{"message":"ciao da a, b","sender":{"email":"u@u.u","passwordHASH":3246578},"recipient":{"email":"e@e.e","passwordHASH":7284563},"date":"2020-12-09 15:46:57.230055"}

{"message":"ciao da a, b","recipient":{"email":"a@a.a","passwordHASH":3246578},"sender":{"email":"b@b.b","passwordHASH":7284563}}



{"message":"ciao da u, u","sender":{"email":"e@u.u","passwordHASH":6232532},"recipient":{"email":"u@u.u","passwordHASH":6232532}}














##DOCUMENTATION AND CODES

-------

#Codes
Code*(JSONObjectType)

code0(message): Message
code1(User): Authentication request
code2(User): Registration Request
code3(StatusCode): StatusCode object
ls: debug command that lists all users registered and connected 

-------

#StatusCodes
0: Generic error
1: Operation successful
2: authentication Error (Will void authentication if sent to a client while authenticated, used in message and authentication form responses)
3: registration Error 
4: formatting Error (Sent due to malformed messages to clients and server)
5: message sending Error, in this case the attachment will be the message (Either the message is not )
6: 