library edo_chat_server;

import 'dart:convert';
import 'dart:io';

import 'package:edo_chat_server/Model/Models.dart';

// USE ALSO netcat 127.0.0.1 3000

// global variables

void main() {
  /*print(json.encode(Message("ciao da a, b", User("a@a.a", 3246578), User("b@b.b", 7284563))));
  print(Message.fromJSONString(json.encode(Message("ciao da a, b", User("a@a.a", 3246578), User("b@b.b", 7284563)))));
*/
  print(Directory.current.path + '\n');
  Server server = Server.getServer();
  print("running server");
}

class Server {
  File archiveFile;
  List<User> totalUsers = [];
  ServerSocket _server;
  List<ChatClient> clients = [];
  static Server _s = Server._internal();

  static Server getServer() => _s;

  Server._internal() {
    ServerSocket.bind(InternetAddress.anyIPv4, 3000)
        .then((ServerSocket socket) {
      _server = socket;
      _server.listen((client) {
        handleConnection(client);
      });
    });
    
    archiveFile= File('../assets/Users.json');
    loadUsers();
  }

  void handleConnection(Socket client) {
    print('Connection from '
        '${client.remoteAddress.address}:${client.remotePort}');

    clients.add(ChatClient(client));
  }

  void removeClient(ChatClient client) {
    clients.remove(client);
  }

  bool authenticate(ChatClient u) {
    if (!containing(u.user)) return false;
    u.authenticated = true;
    return true;
  }

  bool addUser(User u) {
    if (u == null) return false;
    if (containing(u)) return false;
    totalUsers.add(u);
    updateUsers();
    return true;
  }

  void loadUsers() async {
    String response;
    await archiveFile.readAsString().then((value) => response=value);

    if(response==null || response=="") return;
    final parsed = json.decode(response.toString()).cast<Map<String, dynamic>>();
    totalUsers= parsed.map<User>((json) => new User.fromJson(json)).toList();
  }

  void updateUsers() async{
    await archiveFile.writeAsString(json.encode(totalUsers));
  }

  bool containing(User u) {
    for (int i = 0; i < totalUsers.length; i++) {
      if (totalUsers[i].equals(u)) return true;
    }
    return false;
  }

  
  bool sendStatusCode(ChatClient c, String message, int statusCode, dynamic attachment){
    if(statusCode==5 && attachment==null) return false;
    c.write(StatusMessage("Message sent successfully", 0, attachment).toJson().toString());
    return true;
  }
}

// ChatClient class for server

class ChatClient {
  User user;
  Server server = Server.getServer();
  Socket _socket;
  String address;
  int port;
  bool authenticated = false;

  ChatClient(Socket s) {
    _socket = s;
    _socket.listen(messageHandler,
        onError: errorHandler, onDone: finishedHandler);
    address = _socket.remoteAddress.address;
    port = _socket.remotePort;
  }

  void messageHandler(data) {
    String message = utf8.decode(data).trim(), code;

    if (message.length >= 5) {
      code = message.substring(0, 5);
      message = message.substring(5);
    } else code = message.substring(0, message.length);

    switch (code) {
      case "code0": //message
        try {
          sendMessage(Message.fromJSONString(message));
        } catch (FormatException) {
          print("Recieved Malformed json message. Discarding!");
          sendStatusMessage("Malformed message.", 4, null);
        }

        break;
      case "code1": //auth
        try {
          this.user = User.fromJSONString(message);
          if (server.authenticate(this)) {
            sendStatusMessage("Authentication successful", 1, null);
          } else {
            sendStatusMessage("Authentication Error", 2, null);
            this.user = null;
          }
        } catch (FormatException) {
          print("Recieved Malformed message. Discarding!");
          sendStatusMessage("Malformed message.", 4, null);
        }

        break;
      case "code2": //create acc
        print(User.fromJSONString(message).toString());
        try {
          if (server.addUser(User.fromJSONString(message))) {
            sendStatusMessage("Creation successful", 1, null);
          } else {
            sendStatusMessage("Creation Error", 3, null);
          }
        } catch (FormatException) {
          print("Recieved Malformed message. Discarding!");
          sendStatusMessage("Malformed message.", 4, null);
        }

        break;

      case "code3": //StatusCode

        break;
      case "ls": //lists properties
        String s = "TotalUsers:\n";
        for (int i = 0; i < server.totalUsers.length; i++) {
          s += i.toString() +
              ' ' +
              server.totalUsers[i].email +
              ' - ' +
              server.totalUsers[i].passwordHASH.toString() +
              '\n';
        }
        s += "\nConnectedClients: \n";
        for (int i = 0; i < server.clients.length; i++) {
          if (server.clients[i].user != null)
            s += i.toString() +
                ' ' +
                server.clients[i].user.email +
                ' - authenticated: ' +
                server.clients[i].authenticated.toString() +
                '\n';
        }
        this.write(s);

        break;
      default:
        print("Recieved Malformed message. Discarding!");
        sendStatusMessage("Malformed message.", 4, null);
    }

    //server.distributeMessage(this, '${_address}:${_port} Message: $message');
  }

  void errorHandler(error) {
    print('$address:$port Error: $error');
    server.removeClient(this);
    _socket.close();
  }

  void finishedHandler() {
    print('$address:$port Disconnected');
    server.removeClient(this);
    _socket.close();
  }

  void write(String message) {
    _socket.write(message+"\n");
  }

  bool sendStatusMessage(String message, int statusCode, dynamic attachment){
    if(statusCode==5 && attachment==null) return false;
    this.write("code3"+jsonEncode(StatusMessage(message, statusCode, attachment).toJson()).toString());
    return true;
  }

  bool sendMessage(Message m) {
    if(!m.sender.equals(this.user.toUserID())) return sendStatusMessage("Sender corresponds to recipient", 5, m);
    if(!this.authenticated) return sendStatusMessage("Sender isn't authenticated.", 5, m);
    for(ChatClient c in server.clients){
      if(c.user.toUserID().equals(m.recipient)){
        c.write("code0" + json.encode(m));
        StatusMessage("Message sent successfully", 0, null);
        return true;
      }
    }

    return sendStatusMessage("User not found or authenticated.", 5, m);
  }


}
