import 'package:edochat/Model/Client.dart';
import 'package:edochat/Model/Models.dart';
import 'package:edochat/View/Widgets/ConversationWidget.dart';
import 'package:flutter/material.dart';

import 'AppbarWidget.dart';

class ConversationListWidget extends StatefulWidget {
  @override
  _ConversationListWidgetState createState() => _ConversationListWidgetState();
}

class _ConversationListWidgetState extends State<ConversationListWidget> {
  Client client = Client.client;
  TextEditingController mailController = TextEditingController();

  onMessageRecieved() {
    setState(() {});
  }

  _ConversationListWidgetState() {
    client.messageNotifier=onMessageRecieved;
  }

  addChat() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text("Insert email"),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        controller: mailController,
                        autocorrect: false,
                        cursorColor: Theme.of(context).primaryColor,
                        decoration: InputDecoration(
                            fillColor: Theme.of(context).primaryColor,
                            hoverColor: Theme.of(context).primaryColor,
                            focusColor: Theme.of(context).primaryColor,
                            hintText: 'Email...'),
                      ),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.send_rounded),
                    onPressed: () async => {
          Navigator.pop(context),
          await selectChat(Conversation(UserID(mailController.text))),
          },
                    color: Theme.of(context).accentColor,
                  ),
                ],
              ),
              Text(""),
            ],
          );
        });
  }

  selectChat(Conversation c) async {
    client.openConvo = c;
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) => ConversationWidget(c)));
    client.messageNotifier=onMessageRecieved;
    setState(() {
    });
    client.openConvo=null;
  }

/**/

  @override
  Widget build(BuildContext context) {
    client.openConvo = null;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(Icons.chat_rounded),
        onPressed: addChat,
      ),
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).accentColor,
      appBar: appBarWidget(DropdownButton(items: null, onChanged: null), null),
      body: Center(
          child: ListView.builder(
        padding: const EdgeInsets.all(1),
        itemCount: client.convos.length,
        itemBuilder: (BuildContext context, int index) {
          if (client.convos.length == 0) {
            return Card();
          }
          return InkWell(
            onTap: () => {
              client.convos.elementAt(client.convos.length - index - 1).read =
                  true,
              selectChat(
                  client.convos.elementAt(client.convos.length - index - 1))
            },
            child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0)),
                elevation: 8.0,
                margin:
                    new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                child: Container(
                  decoration: BoxDecoration(),
                  child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    leading: Container(
                      padding: EdgeInsets.only(right: 12.0),
                      decoration: new BoxDecoration(
                          border:
                              new Border(right: new BorderSide(width: 1.0))),
                      child: (client.convos
                              .elementAt(client.convos.length - index - 1)
                              .read)
                          ? Icon(Icons.mark_email_read)
                          : Icon(Icons.mark_email_unread),
                    ),
                    title: Text(
                      client.convos
                          .elementAt(client.convos.length - index - 1)
                          .userId
                          .email,
                      style: Theme.of(context).textTheme.headline5,
                    ),
                    subtitle: (client.convos
                                .elementAt(client.convos.length - index - 1)
                                .convo
                                .length ==
                            0)
                        ? Text("No messages")
                        : Text(
                            client.convos
                                .elementAt(client.convos.length - index - 1)
                                .convo
                                .elementAt(client.convos
                                        .elementAt(
                                            client.convos.length - index - 1)
                                        .convo
                                        .length -
                                    1)
                                .message,
                            style: Theme.of(context).textTheme.caption,
                          ),
                  ),
                )),
          );
        },
      )),
    );
  }
}
