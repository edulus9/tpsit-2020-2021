import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:morememo/Data/Models.dart';
import 'package:morememo/Domain/Constants.dart';
import 'package:morememo/Domain/Firestore.dart';

class TagWidget extends StatefulWidget {
  List<Tag> catArr;
  TagWidget(this.catArr);
  @override
  _TagWidgetState createState() => _TagWidgetState(catArr);
}

class _TagWidgetState extends State<TagWidget> {
  List<Tag> catArr;
  FirestoreHandler handler = FirestoreHandler.instance;

  _TagWidgetState(this.catArr);

  onPop() {
    Navigator.pop(context);
  }

  Widget builder(BuildContext context, int index) {
    return ListTile(
        leading: Text(catArr[index].name),
        title: ButtonBar(
          children: [
            IconButton(
                onPressed: (){onModify(catArr[index]);},
                icon: Icon(LineAwesomeIcons.edit)),
            IconButton(
                onPressed: (){onDelete(catArr[index]);},
                icon: Icon(LineAwesomeIcons.remove)),
          ],
        ));
  }

  onAdd() async{
    print("onAdd pressed.");
    TextEditingController nameController=TextEditingController();
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0),
              side: BorderSide(
                  color: Theme.of(context).primaryColor,
                  width: 1
              ),),
            title: Text("New tag"),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        controller: nameController,
                        autocorrect: false,
                        cursorColor: Theme.of(context).primaryColor,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(15))),

                            contentPadding: EdgeInsets.all(10)
                        ),
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(5),
                      child:
                      FloatingActionButton(
                        child: Icon(LineAwesomeIcons.check),
                        onPressed: () async {
                          Navigator.pop(context);
                          await handler.addTag(nameController.text).then((value) {
                            print(value);
                            setState(() {
                              catArr.add(value);
                            });
                          });

                        },
                        //color: Theme.of(context).accentColor,
                      )),
                ],
              ),
              Text(""),
            ],
          );
        });
  }
  onDelete(Tag c) {
    print("Calling deleteTag");
    handler.deleteTag(c);
    handler.getTags().then((value){
      setState(() {
        catArr=value;
      });
    });
  }

  onModify(Tag c) async {
    print("Calling modifyTag");
    TextEditingController nameController=TextEditingController();
    nameController.text=c.name;
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text("Modify tag"),
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: TextField(
                        controller: nameController,
                        autocorrect: false,
                        cursorColor: Theme.of(context).primaryColor,
                        decoration: InputDecoration(
                            fillColor: Theme.of(context).primaryColor,
                            hoverColor: Theme.of(context).primaryColor,
                            focusColor: Theme.of(context).primaryColor,
                            hintText: 'Name...'),
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(5),
                      child: FloatingActionButton(
                        child: Icon(LineAwesomeIcons.check),
                        onPressed: () async {
                          Navigator.pop(context);
                          setState(() {
                            c.name=nameController.text;
                          });
                          handler.setTag(c);
                        },
                        //color: Theme.of(context).accentColor,
                      )),
                ],
              ),
              Text(""),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(LineAwesomeIcons.arrow_left),
          onPressed: onPop,
        ),
        centerTitle: true,
        title: Text("Edit Tags"),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(borderRadius),
              bottomRight: Radius.circular(borderRadius),
            )),
      ),
      body: ListView.builder(
        itemBuilder: builder,
        itemCount: catArr.length,
      ),
      floatingActionButton: ButtonBar(
        children: [
          FloatingActionButton(heroTag: null,onPressed: onAdd, child: Icon(LineAwesomeIcons.plus),),
        ],
      ),
    );
  }
}
