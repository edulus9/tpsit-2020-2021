import 'package:flutter/material.dart';

Widget appBarWidget(DropdownButton d, String title){
    return AppBar( title: Text((title==null)?'Pistachio':title, style: TextStyle(fontStyle: FontStyle.italic)), actions: [DropdownButtonHideUnderline(child: d)]);

}