import 'package:flutter/material.dart';
import 'package:gsin_test/DAOs/FraseCelebreDAO.dart';
import 'package:floor/floor.dart';
import 'package:gsin_test/DBCreation/FraseDB.dart';
import 'package:gsin_test/entities/FraseCelebreEntity.dart';

import 'State.dart';

class DBWidget extends StatelessWidget {
  TextEditingController phraseController=TextEditingController();
  final AppState appState;

  DBWidget({Key key, this.appState}) : super(key: key);

  addFrase() async{
    final database = await $FloorFraseDB.databaseBuilder('FraseDB.db').build();
    final personDao = database.frasecelebreDao;

    final person = FrasiCelebri(1, 'Frank', appState.user.uid.toString());
    await personDao.insertPerson(person);

    final result = await personDao.findPersonById(1);
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          TextField(
            controller: phraseController,
          ),
          FloatingActionButton(onPressed: addFrase),
          FloatingActionButton(onPressed: addFrase),
        ],
      ),
    );
  }
}
