import 'package:floor/floor.dart';

@entity
class User {
  @primaryKey
  final String FirebaseId;
  final String Name;

  User(this.FirebaseId, this.Name);
}