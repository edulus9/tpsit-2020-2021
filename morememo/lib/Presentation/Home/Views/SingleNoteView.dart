import 'package:flutter/Material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:morememo/Data/Note.dart';
import 'package:morememo/Presentation/Home/Views/EditNote.dart';

class SingleNoteWidget extends StatefulWidget {
  Note note;
  @override
  SingleNoteWidget(this.note) {}
  _SingleNoteWidgetState createState() => _SingleNoteWidgetState(note);
}

class _SingleNoteWidgetState extends State<SingleNoteWidget> {
  Note note;

  _SingleNoteWidgetState(this.note);

  @override
  Widget build(BuildContext context) {
    //Markdown()
    print(note.tags);
    return InkWell(
      onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => EditNote(note))).then((value) {setState(() {

              });});
      },
      child: Card(
          shadowColor: Colors.black12,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0),
                  side: BorderSide(
                  color: Theme.of(context).primaryColor,
          width: 1
      ),),
          elevation: 8.0,
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(),
            child: ListTile(

              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              /*leading: Container(
                padding: EdgeInsets.only(right: 12.0),
                decoration: new BoxDecoration(
                    border:
                    new Border(right: new BorderSide(width: 1.0))),

              ),*/
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    note.title,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.headline5,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Card(
                    color: Theme.of(context).primaryColor,
                    shadowColor: Colors.black12,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),

                    ),
                    elevation: 8.0,
                    margin:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                    child: Row(
                      children: [
                        (note.category!=null) ? Container( padding: EdgeInsets.all(5),child: Text(note.category.name)) : Container(width: 0,),
                        (note.category!=null) ? Icon(LineAwesomeIcons.folder, size: 20,) : Container(width: 0,),
                        (!note.tags.isEmpty) ? Icon(LineAwesomeIcons.tags, size: 20,) : Container(width: 0,),
                      ],
                    ),
                  ),
                ],
              ),
              subtitle: Container(
                  height: 50,
                  child: Markdown(
                    physics: NeverScrollableScrollPhysics(),
                    selectable: false,
                    data: note.content,
                  )),
            ),
          )),
    );
  }
}
