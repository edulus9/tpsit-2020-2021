const { randomInt } = require("crypto");
var express = require("express");
var app = express();

class food{
    name;
    category;

    constructor(name, category){
        this.name=name;
        this.category=category;
    }

    equals(food){
        return (this.category==food.category && this.name==food.name);
    }
}

var foods = [
    new food("Panino con Cotoletta", "SecondCourse"), new food("Bistecca", "SecondCourse"), new food("Bastoncini di Merluzzo", "SecondCourse"),
    new food("Torta", "Dessert"), new food("Panna Cotta", "Dessert"), new food("Yogurt", "Dessert"), 
    new food("Carote al vapore", "Vegetable"), new food("Zucchine", "Vegetable"), new food("Melanzane ai ferri", "Vegetable"), 
    new food("Carbonara", "FirstCourse"), new food("Amatriciana", "FirstCourse"), new food("Tortellini alla Macciantelli", "FirstCourse"), 
];


app.listen(3000, () => {

    console.log("Server running on port 3000");
});

app.get("/dailymenu.json", (req, res, next) => {
    var tmpMenu=["","","","",];
    console.log("Randomizing");
    tmpMenu[0]=getRandomFood("FirstCourse");
    console.log("FirstCourse");
    tmpMenu[1]=getRandomFood("SecondCourse");
    console.log("SecondCourse");
    tmpMenu[2]=getRandomFood("Vegetable");
    console.log("Vegetable");
    tmpMenu[3]=getRandomFood("Dessert");
    console.log("Finished Randomizing");

    res.json(tmpMenu);
    console.log("Finished.");
   });

function getRandomFood(category){
    var rndInt;
    do{
        rndInt=Math.floor(Math.random() * foods.length);
    }while(foods[rndInt].category===category);
    return foods[rndInt];

}



