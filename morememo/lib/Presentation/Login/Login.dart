import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:morememo/State.dart';

import '../../GSIn.dart';

class _LoginLoading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SpinKitChasingDots(color: Theme.of(context).primaryColor));
  }
}

class _LoginError extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Icon(Icons.error_outline),
        Text("Error. Something went wrong!")
      ],
    ));
  }
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  AppState appState;
  TextEditingController userController = TextEditingController();
  TextEditingController passController = TextEditingController();
  String authErrorMessage = "Authentication Error. Wrong credentials?";
  bool errorVisibility = false, loadingVisibility = false;

  _LoginState() {
    initState();
  }

  onLogin() async {
    setState(() {
      appState.isLoading = true;
    });

    User firebaseUser;
    await loginWithGoogle().then((value) {
      Navigator.popAndPushNamed(context, "/noteHome");
      firebaseUser=value;
    });

    if (appState.user != firebaseUser) {
      setState(() {
        appState.isLoading = false;
        appState.user = firebaseUser;
        appState.isLogged = true;
      });
    }
    print("Popping!!!!!");
    Navigator.popAndPushNamed(context, "/noteHome");
  }

  alreadyLoggedIn(User tmp) async {
    print("USER: " + tmp.email); //****DEBUG****
    appState.isLogged = true;

    User firebaseUser = await loginWithGoogle();
    appState.isLoading = false;
    appState.user = firebaseUser;
    appState.isLogged = true;
    Navigator.popAndPushNamed(context, "/noteHome");
  }

  initState() {
    appState = AppState();

    User tmp;

    tmp = FirebaseAuth.instance.currentUser;

    appState.user = tmp;
    if (tmp != null) {
      if (!tmp.isAnonymous) {
        alreadyLoggedIn(tmp);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (appState.isLoading)
      return Scaffold(
        body: _LoginLoading(),
      );
    return Scaffold(
        //resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        backgroundColor: Theme.of(context).primaryColor,
        body: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 80),
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 70,
                      height: 70,
                      child: Image.asset("assets/icons/blueberry.png"),
                    ),
                    TextField(
                      controller: userController,
                      autocorrect: false,
                      showCursor: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(15))),
                          contentPadding: EdgeInsets.all(10),

                          icon: Icon(Icons.email_outlined),
                        hintText: 'Username'),
                    ),
                    TextField(
                      controller: passController,
                      autocorrect: false,
                      obscureText: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(15))),
                          contentPadding: EdgeInsets.all(10),

                          icon: Icon(LineAwesomeIcons.key),
                          hintText: 'Password'),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Visibility(
                          visible: errorVisibility,
                          child: Text(authErrorMessage,
                              style: TextStyle(color: Colors.red)),
                        ),
                        /*SizedBox(
                          height: 50,
                          width: 50,
                          child: Visibility(
                            visible: loadingVisibility,
                            child: SpinKitChasingDots(
                                color: Theme.of(context).primaryColor,
                                size: 50.0),
                          ),
                        )*/
                      ],
                    ),
                    ElevatedButton(
                      //LogIn
                      style: ElevatedButton.styleFrom(
                          primary: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0))),
                      child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Align(
                            alignment: Alignment.center,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                    width: 40,
                                    child: Image.asset(
                                        "assets/icons/google-logo.png")),
                                Text(
                                  'Google Sign In',
                                  style: Theme.of(context).textTheme.headline5,
                                ),
                              ],
                            ),
                          )),
                      onPressed: (appState.isLoading == true) ? null : onLogin,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ElevatedButton(
                        //SignUp
                        style: ElevatedButton.styleFrom(
                            primary: Theme.of(context).accentColor,
                            onPrimary: Theme.of(context).accentColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0))),
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              'Login',
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          ),
                        ),
                        onPressed: (){},
                      ),
                        ElevatedButton(
                          //SignUp
                          style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).accentColor,
                              onPrimary: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(30.0))),
                          child: Container(
                            padding: const EdgeInsets.all(8.0),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                'Signup',
                                style: Theme.of(context).textTheme.headline5,
                              ),
                            ),
                          ),
                          onPressed: (){},
                        ),],
                    ),


                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
