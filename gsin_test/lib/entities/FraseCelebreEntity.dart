import 'package:floor/floor.dart';

@entity
class FrasiCelebri{
  @PrimaryKey(autoGenerate: true)
  final int Id;
  final String Frase;
  final int UserId;

  FrasiCelebri(this.Id, this.Frase, this.UserId);
}