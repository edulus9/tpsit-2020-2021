import 'package:bubble/bubble.dart';
import 'package:edochat/Model/Client.dart';
import 'package:edochat/Model/Models.dart';
import 'package:edochat/View/Widgets/AppbarWidget.dart';
import 'package:flutter/material.dart';

class ConversationWidget extends StatefulWidget {
  Conversation co;
  ConversationWidget(Conversation c) {
    co = c;
  }

  @override
  _ConversationWidgetState createState() => _ConversationWidgetState(co);
}

class _ConversationWidgetState extends State {
  Conversation convo;
  Client client=Client.client;
  TextEditingController messController= TextEditingController();

  onMessageRecieved(){
    setState(() {

    });
  }

  _ConversationWidgetState(Conversation c) {
    convo = c;
    c.read=true;
    client.messageNotifier=onMessageRecieved;
  }

  void sendMessage(){setState(() {
    setState(() {
      if(messController.text!=""){
        print("sending message");
        client.sendMessage(Message(messController.text, client.u.toUserID(), convo.userId, DateTime.now().toString()), convo);
        messController.clear();
      }
    });

  });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarWidget(DropdownButton(items: [DropdownMenuItem(onTap: ()=>client.deleteConversation(convo), child: Text("Delete"))], onChanged: null, icon: Icon(Icons.menu_rounded),), convo.userId.email),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(child: ListView.builder(
            reverse: true,

            padding: const EdgeInsets.all(8),
            itemCount: convo.convo.length,
            itemBuilder: (BuildContext context, int index) {
              int i=convo.convo.length - index-1;
              bool isMe=convo.convo[i].sender.equals(client.u.toUserID());
              if(convo.convo.length==0) return Container();
              return Bubble(
                margin: BubbleEdges.only(top: 10),
                color: (isMe) ? Theme.of(context).accentColor : Theme.of(context).cardColor,
                alignment: (isMe)? Alignment.centerRight : Alignment.centerLeft,
                nip: (isMe)? BubbleNip.rightBottom : BubbleNip.leftBottom,
                child: Text(convo.convo[i].message),
              );
            },
          )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: TextField(
                  controller: messController,
                  autocorrect: true,
                  cursorColor: Theme.of(context).primaryColor,
                  decoration: InputDecoration(
                    fillColor: Theme.of(context).primaryColor,
                      hoverColor: Theme.of(context).primaryColor,
                      focusColor: Theme.of(context).primaryColor,
                      hintText: 'Message...'),
              ),
                ),),

              IconButton(icon: Icon(Icons.send_rounded), onPressed: sendMessage, color: Theme.of(context).primaryColor,),
            ],
          ),
        ],
      ),
    );
  }
}
/* (convo.convo[index].sender.equals(convo.userId)) ? Align(
                  alignment: Alignment.centerRight,
                  child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50.0)),
                      elevation: 2.0,
                      margin: new EdgeInsets.symmetric(horizontal: 0.0, vertical: 6.0),
                      child: Container(
                        decoration: BoxDecoration(),
                        child: ListTile(
                          contentPadding:
                          EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                          title: Text(
                            convo.convo[convo.convo.length - index-1].message,
                          ),
                        ),
                      )),
                ) : Align(

                  alignment: Alignment.centerRight,
              child: Card(color: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0)),
              elevation: 8.0,
              margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
              decoration: BoxDecoration(),
              child: ListTile(
              contentPadding:
              EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              title: Text(
              convo.convo[convo.convo.length - index-1].message,
              ),
              ),
              )),
              );*/
/*class Bubble extends StatelessWidget {
  Bubble(this.message){
    isMe=c.u.toUserID().equals(message.sender);
  }

  Client c=Client.client;
  Message message;
  bool isMe;

  @override
  Widget build(BuildContext context) {
    final bg = isMe ? Colors.white : Colors.greenAccent.shade100;
    final align = isMe ? CrossAxisAlignment.start : CrossAxisAlignment.end;
    final radius = isMe
        ? BorderRadius.only(
      topRight: Radius.circular(5.0),
      bottomLeft: Radius.circular(10.0),
      bottomRight: Radius.circular(5.0),
    )
        : BorderRadius.only(
      topLeft: Radius.circular(5.0),
      bottomLeft: Radius.circular(5.0),
      bottomRight: Radius.circular(10.0),
    );
    return Column(
      crossAxisAlignment: align,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(3.0),
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  blurRadius: .5,
                  spreadRadius: 1.0,
                  color: Colors.black.withOpacity(.12))
            ],
            color: bg,
            borderRadius: radius,
          ),
          child: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 48.0),
                child: Text(message.message),
              ),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                child: Row(
                  children: <Widget>[
                    Text(message.date,
                        style: TextStyle(
                          color: Colors.black38,
                          fontSize: 10.0,
                        )),
                    SizedBox(width: 3.0),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}*/
