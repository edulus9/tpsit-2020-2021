import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:my_menu/features/myMenu/Data/ServerHandler.dart';
import 'package:my_menu/features/myMenu/Data/SettingsManager.dart';
import 'package:my_menu/features/myMenu/Presentation/Homepage.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({Key key}) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  Widget downloadWidget;
  ServerHandler server = ServerHandler();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  onDownloadFailed() {}

  Widget getDownloadWidget() {
    setState(() {});

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(Icons.download_rounded, color: Theme.of(context).canvasColor,),
        Text('Download Menù',),
        /*Container(
            child: SpinKitChasingDots(
                color: Theme.of(context).canvasColor, size: 20.0))*/
      ],
    );
  }

  void onDownloadPressed() {
    setState(() {
      if (server.isDownloading) {
      } else {

        server.Download(SettingsManager.URLPreference);
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Card(),
          Container(
            width: 300,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0))),
                onPressed: onDownloadPressed,
                child: getDownloadWidget()),
          ),
          Text('')
        ]),
      ),
    );
  }
}
