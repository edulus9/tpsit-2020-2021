import 'package:cloud_firestore/cloud_firestore.dart';

class Tag{
  DocumentReference reference;
  final String UID;
  String name;

  Tag(this.name, this.UID, this.reference);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Tag && runtimeType == other.runtimeType && UID == other.UID;

  @override
  int get hashCode => UID.hashCode;

  @override
  String toString() {
    return 'Tag{UID: $UID, name: $name}';
  }
}

class Category{
  DocumentReference reference;
  final String UID;
  String name;

  Category(this.name, this.UID, this.reference);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Category && runtimeType == other.runtimeType && UID == other.UID;

  @override
  int get hashCode => UID.hashCode;

  @override
  String toString() {
    return 'Category{UID: $UID, name: $name}';
  }
}