import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:morememo/Data/Models.dart';
import 'package:morememo/Data/Note.dart';
import 'package:morememo/Domain/Constants.dart';
import 'package:morememo/Domain/Firestore.dart';
import 'package:morememo/Presentation/Home/Views/CategoryWidget.dart';

import 'TagWidget.dart';

class SummaryWidget extends StatefulWidget {
  @override
  _SummaryWidgetState createState() => _SummaryWidgetState();
}

class _SummaryWidgetState extends State<SummaryWidget> {
  FirestoreHandler handler=FirestoreHandler.instance;
  List<Category> categories=[];
  List<Note> notes=[];
  String name=FirebaseAuth.instance.currentUser.displayName.substring(0, FirebaseAuth.instance.currentUser.displayName.indexOf(" "));


  _SummaryWidgetState(){
    handler.getCategories().then((value) {
      categories = value;
      handler.getNotes().then((value) {
        notes=value;
        setState(() {
        });
      });
    });

  }

  onCategoriesPressed()async{
    List<Category> l;
    await handler.getCategories().then((value) => l=value);
    Navigator.push(context, MaterialPageRoute(builder: (context)=>CategoryWidget(l)));
  }
  onTagsPressed()async {
    List<Tag> l;
    await handler.getTags().then((value) => l=value);
    Navigator.push(context, MaterialPageRoute(builder: (context)=>TagWidget(l)));

  }

  @override
  Widget build(BuildContext context) {
    TextStyle headlineGreeting=Theme.of(context).textTheme.headline6;
    return Scaffold(
      appBar: AppBar(
        leading: Icon(null),
        centerTitle: true,
        title: Text("Hello, $name", style: TextStyle(
          color: Colors.white,
          fontStyle: headlineGreeting.fontStyle
        )),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(borderRadius),
              bottomRight: Radius.circular(borderRadius),
            )),
      ),
      body: Stack(
        children: [

          Align(
            alignment: Alignment.topLeft,
            child: Card(
                //color: Theme.of(context).primaryColor,
                shadowColor: Colors.black12,
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                        color: Theme.of(context).accentColor,
                        width: 2
                    ),
                    borderRadius: BorderRadius.circular(borderRadius)),
                elevation: 8.0,
                margin:
                new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                child: Container(
                  decoration: BoxDecoration(),
                  child: ListTile(
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    /*leading: Container(
                padding: EdgeInsets.only(right: 12.0),
                decoration: new BoxDecoration(
                    border:
                    new Border(right: new BorderSide(width: 1.0))),

              ),*/
                    title: Text("Your stats!"),
                    subtitle: Container( height: 70, child:Column( crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text("Total notes: ${notes.length}"),
                        Text("Shared notes: UNIMPL"),
                        Text("Number of categories: ${categories.length}"),
                      ],
                    ),
                    ),
                    leading: Container( height: 70, child: Icon(Icons.bar_chart, size: 70,)),
                  ),
                )),
          ),
        ],
      ),
    floatingActionButton: ButtonBar(

      children: [
        FloatingActionButton(heroTag: null,onPressed: onTagsPressed, child: Icon(LineAwesomeIcons.tags),),
        FloatingActionButton(heroTag: null,onPressed: onCategoriesPressed, child: Icon(LineAwesomeIcons.folder),),
      ],
    ),
    );
  }
}
