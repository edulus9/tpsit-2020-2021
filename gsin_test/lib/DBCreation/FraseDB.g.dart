// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FraseDB.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorFraseDB {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$FraseDBBuilder databaseBuilder(String name) =>
      _$FraseDBBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$FraseDBBuilder inMemoryDatabaseBuilder() => _$FraseDBBuilder(null);
}

class _$FraseDBBuilder {
  _$FraseDBBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$FraseDBBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$FraseDBBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<FraseDB> build() async {
    final path = name != null
        ? await sqfliteDatabaseFactory.getDatabasePath(name)
        : ':memory:';
    final database = _$FraseDB();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$FraseDB extends FraseDB {
  _$FraseDB([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  FraseCelebreDAO _frasecelebreDaoInstance;

  UserDAO _userDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    final databaseOptions = sqflite.OpenDatabaseOptions(
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `FrasiCelebri` (`Id` INTEGER PRIMARY KEY AUTOINCREMENT, `Frase` TEXT, `UserId` INTEGER)');
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `User` (`FirebaseId` TEXT, `Name` TEXT, PRIMARY KEY (`FirebaseId`))');

        await callback?.onCreate?.call(database, version);
      },
    );
    return sqfliteDatabaseFactory.openDatabase(path, options: databaseOptions);
  }

  @override
  FraseCelebreDAO get frasecelebreDao {
    return _frasecelebreDaoInstance ??=
        _$FraseCelebreDAO(database, changeListener);
  }

  @override
  UserDAO get userDao {
    return _userDaoInstance ??= _$UserDAO(database, changeListener);
  }
}

class _$FraseCelebreDAO extends FraseCelebreDAO {
  _$FraseCelebreDAO(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _frasiCelebriInsertionAdapter = InsertionAdapter(
            database,
            'FrasiCelebri',
            (FrasiCelebri item) => <String, dynamic>{
                  'Id': item.Id,
                  'Frase': item.Frase,
                  'UserId': item.UserId
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<FrasiCelebri> _frasiCelebriInsertionAdapter;

  @override
  Future<List<FrasiCelebri>> getAllFrasiCelebri() async {
    return _queryAdapter.queryList('SELECT * FROM FrasiCelebri',
        mapper: (Map<String, dynamic> row) => FrasiCelebri(
            row['Id'] as int, row['Frase'] as String, row['UserId'] as int));
  }

  @override
  Future<void> insertFrasiCelebri(FrasiCelebri frasiCelebri) async {
    await _frasiCelebriInsertionAdapter.insert(
        frasiCelebri, OnConflictStrategy.abort);
  }
}

class _$UserDAO extends UserDAO {
  _$UserDAO(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database),
        _userInsertionAdapter = InsertionAdapter(
            database,
            'User',
            (User item) => <String, dynamic>{
                  'FirebaseId': item.FirebaseId,
                  'Name': item.Name
                });

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  final InsertionAdapter<User> _userInsertionAdapter;

  @override
  Future<List<User>> getAllUsers() async {
    return _queryAdapter.queryList('SELECT * FROM Users',
        mapper: (Map<String, dynamic> row) =>
            User(row['FirebaseId'] as String, row['Name'] as String));
  }

  @override
  Future<void> insertUser(User user) async {
    await _userInsertionAdapter.insert(user, OnConflictStrategy.abort);
  }
}
