import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

final GoogleSignIn _googleSignIn = GoogleSignIn();
final FirebaseAuth _auth = FirebaseAuth.instance;

Future<FirebaseUser> loginWithGoogle() async {
  FirebaseUser user;
  if(_googleSignIn.currentUser != null) {
    return _auth.currentUser();
  }
  GoogleSignInAccount googleUser = await _googleSignIn.signIn();
  GoogleSignInAuthentication googleAuth = await googleUser.authentication;
  AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );
  user = await _auth.signInWithCredential(credential);
  print("signed in " + user.displayName);
  return user;
}

Future<Null> logoutWithGoogle() async {
  await _auth.signOut();
  await _googleSignIn.signOut();
}

/*
Future<Null> deleteUserWithGoogle(FirebaseUser user) async {
  await user.delete();
  await logoutWithGoogle();
}
*/
