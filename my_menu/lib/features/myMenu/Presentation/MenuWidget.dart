import 'package:flutter/material.dart';
import 'package:my_menu/features/myMenu/Data/Food.dart';
import 'package:my_menu/features/myMenu/Presentation/Homepage.dart';

class MenuWidget extends StatefulWidget {
  MenuWidget({Key key}) : super(key: key);

  @override
  _MenuWidgetState createState() => _MenuWidgetState();
}

class _MenuWidgetState extends State<MenuWidget> {

  List<Food> courses;



  String getFood(Category category){

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            'Today\'s Menu: ',
            style: Theme.of(context).textTheme.headline3,
          ),
          Card(
            child: Container(
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.yellow,
                    ),
                    Text(
                      'pasta bla bla',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ],
                )),
          ),
          Card(
            child: Container(
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.yellow,
                    ),
                    Text(
                      'pasta bla bla',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ],
                )),
          ),
          Card(
            child: Container(
                height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      color: Colors.yellow,
                    ),
                    Text(
                      'pasta bla bla',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }
}
