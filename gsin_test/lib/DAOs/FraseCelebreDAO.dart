import 'package:floor/floor.dart';
import 'package:gsin_test/entities/FraseCelebreEntity.dart';

@dao
abstract class FraseCelebreDAO {

  @Query("SELECT * FROM FrasiCelebri")
  Future<List<FrasiCelebri>> getAllFrasiCelebri();

  @insert
  Future<void> insertFrasiCelebri(FrasiCelebri frasiCelebri);

}