import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';
import 'package:morememo/Domain/Firestore.dart';
import 'package:morememo/Presentation/Home/Views/AllNoteView.dart';
import 'package:morememo/Presentation/Home/Views/HomeView.dart';
import 'package:morememo/Presentation/Home/Views/SharedView.dart';
import 'package:morememo/Presentation/Home/Views/UserView.dart';
import 'package:morememo/State.dart';

class HomePageWidget extends StatefulWidget {
  @override
  _HomePageWidgetLoaded createState() => _HomePageWidgetLoaded();
}

class _HomePageWidgetLoaded extends State<HomePageWidget> {
  AppState appState;
  PageController _pageController = PageController(initialPage: 0);
  int _pageSelection=0;


  _HomePageWidgetLoaded(){
    appState=AppState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).primaryColor,
            ),
          ],
        ),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15.0,
              vertical: 10,
            ),
            child: GNav(
              selectedIndex: _pageSelection,
              onTabChange: (int index) {
                print(index);
                _pageSelection = index;
                _pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.linearToEaseOut);
                _pageController.jumpToPage(_pageSelection);
                //setState(() {});
              },
              gap: 4,
              activeColor: Theme.of(context).cardColor,
              iconSize: 24,
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: 5,
              ),
              duration: Duration(
                milliseconds: 800,
              ),
              tabBackgroundColor: Theme.of(context).primaryColor,
              tabs: [
                GButton(
                  icon: LineAwesomeIcons.home,
                  text: 'Home',
                ),
                GButton(
                  icon: LineAwesomeIcons.sticky_note_o,
                  text: 'All notes',
                ),
                GButton(
                  icon: LineAwesomeIcons.send,
                  text: 'Shared',
                ),
                GButton(
                  icon: LineAwesomeIcons.user,
                  text: 'Users',
                ),
              ],
            ),
          ),
        ),
      ),
      body: SafeArea(
        top: true,
        child: PageView(

          scrollDirection: Axis.horizontal,
          controller: _pageController,
          onPageChanged: (index) {
            _pageSelection = index;
            setState(() {

            });
          },
          children: [
            SummaryWidget(),
            AllNoteWidget(),
            SharedWidget(),
            UserWidget(),
          ],
        ),
      ),
      //floatingActionButtonAnimator: ,
    );
  }
}

class _HomePageWidgetLoading extends State<HomePageWidget> {


  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}


