import 'dart:convert';
class User{
  int passwordHASH;
  String email;

  User(String mail, int password){
    this.email=mail;
    this.passwordHASH=password;
  }


  User.fromJson(Map<String, dynamic> json){
    email = json["email"];
    passwordHASH = json["passwordHASH"];}

  User.fromJsonAuth(Map<String, dynamic> json):
        email = json['email'],
        passwordHASH = json['passwordHASH'];


  UserID toUserID(){
    return UserID(this.email);
  }

  @override
  bool equals(User u){
    return this.passwordHASH==u.passwordHASH && this.email==u.email;
  }

  static User fromJSONString(String json){
    Map<String, dynamic> jmap=jsonDecode(json);
    return User(jmap["email"], jmap['passwordHASH']);
  }

  Map<String, dynamic> toJson() =>
      {
        'email': email,
        'passwordHASH': passwordHASH,
      };

  @override
  String toString() {
    return email + " - " +passwordHASH.toString();
  }
}

class Message{
  String message;
  UserID sender;
  UserID recipient;
  String date;

  Message(this.message, this.sender, this.recipient, this.date);

  String toJSONString(){
    return jsonEncode(this);
  }

  static Message fromJSONString(String jsonString){
    Map<String, dynamic> map = jsonDecode(jsonString);

    return Message(map["message"], UserID.fromJSONString(jsonEncode(map["sender"])), UserID.fromJSONString(jsonEncode(map["recipient"])), map['date']);
  }
  Message.fromJSON(Map<dynamic, dynamic> map){
    message=map["message"];
    date=map["date"];
    recipient=UserID.fromJson(map["recipient"]);
    sender=UserID.fromJson(map["sender"]);
  }

  Map<String, dynamic> toJson() =>
      {
        'message': message,
        'sender': sender,
        'recipient': recipient,
        'date': date,
      };

  @override
  String toString() {
    String s="";
    s="{MESSAGE: ${this.message} \nFROM: ${this.sender.toString()} \nTO: ${this.recipient.toString()}}";
    return s;
  }


}

class StatusMessage{
  String message;
  int statusCode;
  dynamic attachment;

  /*
  * statuscodes:
  * 0: generic error
  * 1: successful
  * 2: authError
  * 3: registrationError
  * 4: formattingError
  * 5: messageError
  * */

  StatusMessage(this.message, this.statusCode, this.attachment){
    if(this.statusCode==5 && this.attachment==null){
      throw new FormatException("A statuscode 5 must have a message!");
    }
  }

  Map<String, dynamic> toJson()=>{
    'message': message,
    'statusCode': statusCode,
    'attachment': attachment,
  };

  static StatusMessage fromJSONString(String json){
    Map<String, dynamic> map = jsonDecode(json);
    return StatusMessage(map["message"], map["statusCode"], (map["statusCode"]=="5") ? User.fromJSONString(jsonEncode(map["attachment"])) : null);
  }


}

class Conversation {
  UserID userId;
  List<Message> convo=[];
  bool read=false;

  Conversation(this.userId);

  void addMessage(Message m){
    convo.add(m);
  }

  Conversation.fromJson(Map<String, dynamic> json){
    userId = UserID.fromJson(json["userid"]);
    convo= (json["convo"]).map<Message>((js) => new Message.fromJSON(js)).toList();
    read=json["read"] as bool;
  }

  Map<String, dynamic> toJson()=>{
    "userid" : userId,
    "convo" : convo,
    "read" : read,
  };

  @override
  String toString() {

    return "userID: $userId\n" +
        "Messages: " + convo.toString()
    ;
  }

  @override
  bool operator ==(Object o) {
    if(o==null) return false;
    Conversation c=o as Conversation;
    return userId == c.userId;
  }

}

class UserID{
  String email;

  UserID(String mail){
    this.email=mail;
  }


  UserID.fromJson(Map<String, dynamic> json){
    email = json["email"];}

  UserID.fromJsonAuth(Map<String, dynamic> json):
        email = json['email'];


  @override
  bool equals(UserID u){
    return this.email==u.email;
  }

  static UserID fromJSONString(String json){
    Map<String, dynamic> jmap=jsonDecode(json);
    return UserID(jmap["email"]);
  }

  Map<String, dynamic> toJson() =>
      {
        'email': email,
      };

  @override
  String toString() {
    return email;
  }
}