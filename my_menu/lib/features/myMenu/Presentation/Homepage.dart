import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_menu/features/myMenu/Presentation/HomeWidget.dart';
import 'package:my_menu/features/myMenu/Presentation/MenuWidget.dart';

import 'SettingsWidget.dart';

class HomepageWidget extends StatefulWidget {
  HomepageWidget({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomepageWidget> {
  int _bottomSelection=0;
  List<BottomNavigationBarItem> _bottomNavBarItems = [
    BottomNavigationBarItem(icon: Icon(Icons.home_rounded), label: "Home"),
    BottomNavigationBarItem(icon: Icon(Icons.restaurant_menu_rounded), label: "Menù"),
    BottomNavigationBarItem(icon: Icon(Icons.settings_rounded), label: "Settings"),
  ];
  List<Widget> _widgets=[
    HomeWidget(),
    MenuWidget(),
    SettingsWidget(),
  ];



  void _onBottomItemSelected(int selection){
    setState(() {
      _bottomSelection=selection;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _widgets[_bottomSelection],
      bottomNavigationBar: BottomNavigationBar(
        items: _bottomNavBarItems,
        onTap: _onBottomItemSelected,
        currentIndex: _bottomSelection,
      ),
    );
  }
}
