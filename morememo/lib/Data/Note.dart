import 'package:cloud_firestore/cloud_firestore.dart';

import 'Models.dart';

class Note{
  String title, content, UID;
  DocumentReference reference;
  Category category;
  List<dynamic> tags;
  DateTime dateLastModified, dateCreated;
  List<dynamic> shared;

  Note(this.UID, this.title, this.content, this.category, this.tags, this.dateLastModified, this.dateCreated, this.reference){
    this.shared=[];
  }

  Future<List<Tag>> getTagList() async {
    List<Tag> result=[];

    for(DocumentReference d in tags){
      DocumentSnapshot data;
      await d.get().then((value) => data=value);
      result.add(Tag(data.get("Name"), data.id, d));
    }

    return result;
  }

  @override
  String toString() {
    return 'Note{UID: $UID, title: $title, \ncontent: $content, \ncategory: $category, \ndateLastModified: $dateLastModified, \ndateCreated: $dateCreated}';
  }
}