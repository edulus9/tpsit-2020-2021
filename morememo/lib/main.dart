import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:morememo/Presentation/Home/Homepage.dart';

import 'DataWid.dart';
import 'GSIn.dart';
import 'Presentation/Login/Login.dart';
import 'State.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  MyApp(){

  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    //while(Firebase.apps.isEmpty);

    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Scaffold(body: Center(child: Icon(Icons.error),));
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {

          return MaterialApp(
            darkTheme: ThemeData.dark(),
            debugShowCheckedModeBanner: false,
            navigatorKey: this.key,
            title: 'ET-GSInTest',
            theme: ThemeData(
              primarySwatch: Colors.indigo,
              primaryColor: Colors.indigo[100],
              accentColor: Colors.green[100],
              //primaryColorDark: Colors.indigo[50],
            ),
            initialRoute: "/login",
            routes: {
              "/noteHome": (BuildContext context) => HomePageWidget(),
              "/login": (BuildContext context) => Login(),
            },
            home: /*MyHomePage(title: 'ET-GSInTest')*/ Login(),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return Scaffold(body: Center(child: Container(child: Icon(Icons.timelapse),)));
      },
    );
  }
}
/*
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  AppState appState;

  @override
  void initState(){
    super.initState();
    FirebaseAuth.instance.setPersistence(Persistence.LOCAL);
    appState = AppState();
    User tmp;

      tmp=FirebaseAuth.instance.currentUser;
      appState.user=tmp;
      if(tmp!=null) appState.isLogged=true;
      setState(() {
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (!appState.isLogged) ? GoogleSignInButton(
              borderRadius: 20,
              onPressed: (appState.isLoading == true) ? null : login,
              darkMode: true, // default: false
            ) : Container(),
            (appState.isLogged) ? DataWidget (appState: appState, callback: logout) : Container()
          ],
        ),
      ),
    );
  }

  Future login() async {
    setState(() {
      appState.isLoading = true;
    });

    User firebaseUser = await loginWithGoogle();

    if(appState.user != firebaseUser) {
      setState(() {
        appState.isLoading = false;
        appState.user = firebaseUser;
        appState.isLogged = true;
      });
    }
  }

  // this will be passed to  the subWidget
  void logout() {
    setState(() {
      appState.isLoading = true;
    });


    logoutWithGoogle().whenComplete(() => setState(() {
      appState.isLoading = false;
      appState.user = null;
      appState.isLogged = false;
    }));

  }
}*/
